# Flask-test for DirectLine
This is a simple api, for a simple script that finds the sum of two numbers

## Install requirements
install all the required packages with: `pip install -r requirements.txt`

## Run the app 
-to run the app execute the command `python app.py`<br>
-go to <a href="http://localhost:5000/sumtwonumbers/10/60">http://localhost:5000/sumtwonumbers/10/60</a> you can replace the numbers with the one you want
