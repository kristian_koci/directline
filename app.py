import directSum
import flask
from flask import Flask, request, json
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)
 
class digitalNumbers(Resource):
    def get(self, first_number, second_number):
        return {'data': directSum.sumTwoNumberss(first_number,second_number)}

api.add_resource(digitalNumbers, '/sumtwonumbers/<first_number>/<second_number>')

if __name__ == '__main__':
     app.run()